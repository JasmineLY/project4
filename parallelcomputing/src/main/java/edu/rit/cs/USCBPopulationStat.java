package edu.rit.cs;

// STNAME,CTYNAME,YEAR,AGEGRP,WA_MALE,WA_FEMALE,BA_MALE,BA_FEMALE,IA_MALE,IA_FEMALE,AA_MALE,AA_FEMALE,NA_MALE,NA_FEMALE,TOM_MALE,TOM_FEMALE
public class USCBPopulationStat {
    private String STNAME;
    private String CTYNAME;
    private int YEAR, AGEGRP, WA_MALE, WA_FEMALE, BA_MALE, BA_FEMALE, IA_MALE, IA_FEMALE, AA_MALE, AA_FEMALE, NA_MALE, NA_FEMALE, TOM_MALE, TOM_FEMALE;

    public String getSTNAME() {
        return STNAME;
    }

    public void setSTNAME(String STNAME) {
        this.STNAME = STNAME;
    }

    public String getCTYNAME() {
        return CTYNAME;
    }

    public void setCTYNAME(String CTYNAME) {
        this.CTYNAME = CTYNAME;
    }

    public int getYEAR() {
        return YEAR;
    }

    public void setYEAR(int YEAR) {
        this.YEAR = YEAR;
    }

    public int getAGEGRP() {
        return AGEGRP;
    }

    public void setAGEGRP(int AGEGRP) {
        this.AGEGRP = AGEGRP;
    }

    public int getWA_MALE() {
        return WA_MALE;
    }

    public void setWA_MALE(int WA_MALE) {
        this.WA_MALE = WA_MALE;
    }

    public int getWA_FEMALE() {
        return WA_FEMALE;
    }

    public void setWA_FEMALE(int WA_FEMALE) {
        this.WA_FEMALE = WA_FEMALE;
    }

    public int getBA_MALE() {
        return BA_MALE;
    }

    public void setBA_MALE(int BA_MALE) {
        this.BA_MALE = BA_MALE;
    }

    public int getBA_FEMALE() {
        return BA_FEMALE;
    }

    public void setBA_FEMALE(int BA_FEMALE) {
        this.BA_FEMALE = BA_FEMALE;
    }

    public int getIA_MALE() {
        return IA_MALE;
    }

    public void setIA_MALE(int IA_MALE) {
        this.IA_MALE = IA_MALE;
    }

    public int getIA_FEMALE() {
        return IA_FEMALE;
    }

    public void setIA_FEMALE(int IA_FEMALE) {
        this.IA_FEMALE = IA_FEMALE;
    }

    public int getAA_MALE() {
        return AA_MALE;
    }

    public void setAA_MALE(int AA_MALE) {
        this.AA_MALE = AA_MALE;
    }

    public int getAA_FEMALE() {
        return AA_FEMALE;
    }

    public void setAA_FEMALE(int AA_FEMALE) {
        this.AA_FEMALE = AA_FEMALE;
    }

    public int getNA_MALE() {
        return NA_MALE;
    }

    public void setNA_MALE(int NA_MALE) {
        this.NA_MALE = NA_MALE;
    }

    public int getNA_FEMALE() {
        return NA_FEMALE;
    }

    public void setNA_FEMALE(int NA_FEMALE) {
        this.NA_FEMALE = NA_FEMALE;
    }

    public int getTOM_MALE() {
        return TOM_MALE;
    }

    public void setTOM_MALE(int TOM_MALE) {
        this.TOM_MALE = TOM_MALE;
    }

    public int getTOM_FEMALE() {
        return TOM_FEMALE;
    }

    public void setTOM_FEMALE(int TOM_FEMALE) {
        this.TOM_FEMALE = TOM_FEMALE;
    }

}
