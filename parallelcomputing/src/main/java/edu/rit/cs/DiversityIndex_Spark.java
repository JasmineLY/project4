package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import scala.Tuple2;

import java.io.File;
import java.util.*;

/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-11-29
 */
public class DiversityIndex_Spark {
    public static final String OutputDirectory = "dataset/outputs";
    public static final String DatasetFile = "dataset/USCB.csv";

    /**
     * delete the directory for new files
     *
     * @param directoryToBeDeleted the directory to delete
     * @return if delete success or not
     */
    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }

    /**
     * read the file and select the needed information and cast them
     *
     * @param spark the spark session
     */
    public static void readFile(SparkSession spark) {
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(DatasetFile);
        ds = ds
                .withColumn("STNAME", ds.col("STNAME").cast(DataTypes.StringType))
                .withColumn("CTYNAME", ds.col("CTYNAME").cast(DataTypes.StringType))
                .withColumn("YEAR", ds.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("WA_MALE", ds.col("WA_MALE").cast(DataTypes.IntegerType))
                .withColumn("WA_FEMALE", ds.col("WA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("BA_MALE", ds.col("BA_MALE").cast(DataTypes.IntegerType))
                .withColumn("BA_FEMALE", ds.col("BA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("IA_MALE", ds.col("IA_MALE").cast(DataTypes.IntegerType))
                .withColumn("IA_FEMALE", ds.col("IA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("AA_MALE", ds.col("AA_MALE").cast(DataTypes.IntegerType))
                .withColumn("AA_FEMALE", ds.col("AA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("NA_MALE", ds.col("NA_MALE").cast(DataTypes.IntegerType))
                .withColumn("NA_FEMALE", ds.col("NA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_MALE", ds.col("TOM_MALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_FEMALE", ds.col("TOM_FEMALE").cast(DataTypes.IntegerType));

        // encoders are created for Java beans
        Encoder<USCBPopulationStat> uscbEncoder = Encoders.bean(USCBPopulationStat.class);
        Dataset<USCBPopulationStat> ds1 = ds.as(uscbEncoder);

        ds1.show();

        Dataset<USCBPopulationStat> ds2 = ds1.select("STNAME", "CTYNAME",
                "YEAR", "AGEGRP",
                "WA_MALE", "WA_FEMALE",
                "BA_MALE", "BA_FEMALE",
                "IA_MALE", "IA_FEMALE",
                "AA_MALE", "AA_FEMALE",
                "NA_MALE", "NA_FEMALE",
                "TOM_MALE", "TOM_FEMALE").as(uscbEncoder);

        toRDD(ds2);
    }

    /**
     * make to rdd and work with distributed partitions
     *
     * @param ds the dataset with needed information
     */
    private static void toRDD(Dataset<USCBPopulationStat> ds) {
        // get the data with all record that hasagegrp = 0 in a list
        // idx 0 = state name(STNAME)
        // idx 1 = County name(CTYNAME)
        // idx 2 = Year
        // idx 3 = WA_MALE + WA_FEMALE
        // idx 4 = BA_MALE + BA_FEMALE
        // idx 5 = IA_MALE + IA_FEMALE
        // idx 6 = AA_MALE + AA_FEMALE
        // idx 7 = NA_MALE + NA_FEMALE
        // idx 8 = TOM_MALE + TOM_FEMALE
        // idx 9 = Total
        JavaRDD<List> agegrp0 = ds.toJavaRDD().map(w -> {
            List<Object> list = new ArrayList<Object>();
            if (w.getAGEGRP() == 0) {
                list.add(w.getSTNAME());
                list.add(w.getCTYNAME());
                list.add(w.getYEAR());
                list.add(w.getWA_FEMALE() + w.getWA_MALE());
                list.add(w.getBA_MALE() + w.getBA_FEMALE());
                list.add(w.getIA_MALE() + w.getIA_FEMALE());
                list.add(w.getAA_MALE() + w.getAA_FEMALE());
                list.add(w.getNA_MALE() + w.getNA_FEMALE());
                list.add((w.getTOM_MALE() + w.getTOM_FEMALE()));
                int total = 0;
                for (int i = 3; i < 9; i++) {
                    total += (int) list.get(i);
                }
                list.add(total);
                return list;
            }
            return list;
        });

        agegrp0 = agegrp0.filter(f -> !f.isEmpty());

        // the pair with STNAME and CTYNAME as key and data as value
        JavaPairRDD<Tuple2<String, String>, List> org_pair = agegrp0.mapToPair(w ->
                new Tuple2<>(new Tuple2<>((String) w.get(0), (String) w.get(1)),
                        w.subList(3, w.size()))
        );

        // reduce the map and get the total number for each race and all
        org_pair = org_pair.reduceByKey((a, b) ->
        {
            List<Integer> sum = new ArrayList<>();
            for (int i = 0; i < a.size(); i++) {
                sum.add((int) a.get(i) + (int) b.get(i));
            }
            return sum;
        });

        org_pair = org_pair.sortByKey(new TupleComparator());

        JavaRDD<List<String>> dIdx = org_pair.map(w ->
                {
                    List<String> result = new ArrayList<>();
                    result.add(w._1._1);
                    result.add(w._1._2);
                    result.add(calculateDiversityIdx(w._2.subList(0, w._2.size() - 1),
                            (int) w._2.get(w._2.size() - 1)));
                    return result;
                }
        );

        deleteDirectory(new File(OutputDirectory));
        dIdx.coalesce(1).saveAsTextFile(OutputDirectory);
    }

    /**
     * the formula to calculate the diversity index
     *
     * @param raceTot the list contains tot num of ppl in each racial category
     * @param total   the total number of ppl in this state and county
     * @return the calculated index that convert to string to save
     */
    private static String calculateDiversityIdx(List raceTot, int total) {
        // 1/T^2
        double part1 = 1 / Math.pow((double) total, 2);
        double sum = 0;
        for (int i = 0; i < raceTot.size(); i++) {
            /*number of individuals in this racial category*/
            // Ni
            long num = (int) raceTot.get(i);
            /*the difference of total num of individuals and the number in this racial category*/
            // T - Ni
            long sub = (total - (int) raceTot.get(i));
            /*sum up each racial category*/
            sum += num * sub;
        }
        return Double.toString(part1 * sum);
    }

    public static void main(String[] args) throws Exception {

        // Create a SparkConf that loads defaults from system properties and the classpath
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[8]");
        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("Compute Diversity Index");

        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        readFile(spark);

        // Stop existing spark context
        jsc.close();

        // Stop existing spark session
        spark.close();
    }
}
