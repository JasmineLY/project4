package edu.rit.cs;

import scala.Serializable;
import scala.Tuple2;

import java.util.Comparator;

/**
 * @author Jasmine Liang
 * @email: yxl5521@rit.edu
 * @date: 2019-12-01
 * <p>
 * the class to let the tuple to compare
 * if state name is same then sort by the county name
 * in ascending order
 */
public class TupleComparator implements Comparator<Tuple2<String, String>>, Serializable {
    @Override
    public int compare(Tuple2<String, String> o1, Tuple2<String, String> o2) {
        int stateComp = o1._1.compareTo(o2._1);
        int ctyComp = o1._2.compareTo(o2._2);
        if (stateComp == 0) {
            return ((ctyComp == 0) ? stateComp : ctyComp);
        }
        return stateComp;
    }
}
